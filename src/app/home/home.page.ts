import { AfterViewInit, ChangeDetectorRef, Component, ElementRef, ViewChild } from '@angular/core';
import { VideoService } from '../sercices/video.service';
import { Camera, CameraResultType } from '@capacitor/camera';
//import { CameraPreviewOptions, CameraPreviewPictureOptions } from "@capacitor-community/camera-preview";
import '@capacitor-community/camera-preview';
import {Capacitor, Plugins} from '@capacitor/core';
import * as WebVPPlugin from 'capacitor-video-player';
const { CapacitorVideoPlayer } = Plugins




@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements AfterViewInit {
  mediaRecorder:any;
  videoPlayer:any;
  isRecording = false;
  //videos = [];
  public videos : string[] = [];
  @ViewChild('video')
  captureElement!: ElementRef;

  constructor(private videoService: VideoService, private changeDetector: ChangeDetectorRef) {

  }

  async ngAfterViewInit() {
     //Initialise the video player plugin
     this.videos = await this.videoService.loadVideos();
     if(Capacitor.isNative){
       this.videoPlayer = CapacitorVideoPlayer;

     }else {
      this.videoPlayer = WebVPPlugin.CapacitorVideoPlayer
     }
  }

  async recordVideo() {
    const stream = await navigator.mediaDevices.getUserMedia({
      video: {
        facingMode: 'user'
      },
      audio: true
    });
    this.captureElement.nativeElement.srcObject = stream;
    this.isRecording = true;

    const options = {mimeType: 'video/Webm'};
    this.mediaRecorder = new  MediaRecorder(stream,options)
    //let chunks = [];
    let chunks = new Array();
    this.mediaRecorder.ondataavailable = (event: { data: { size: number; }; }) => {
      if(event.data && event.data.size> 0 ){
        chunks.push(event.data)
      }
    }

    this.mediaRecorder.onstop = async (event: any) => {
      const videoBuffer = new Blob(chunks, {type: 'video/webm' });
      //Store the video
      await this.videoService.storeVideo(videoBuffer);
      //reload the list 
      this.videos = this.videoService.videos;
      this.changeDetector.detectChanges();
    }

  }

  stopRecord(){
    this.mediaRecorder.stop();
    //this.mediaRecorder = null;
    this.captureElement.nativeElement.srcObject = null;
    this.isRecording = false;

  }

  async play(video: any) {
    const base64data = await this.videoService.getVideoUrl(video);
    await this.videoPlayer.initPlayer({
       mode: 'fullscreen',
       url:base64data,
       playerId:'fullscreen',
       ComponentTag: 'app-home'
    })
  }

}
