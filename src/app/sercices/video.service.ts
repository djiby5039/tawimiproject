import { Injectable } from '@angular/core';
import { Plugins } from '@capacitor/core'

import { Preferences } from '@capacitor/preferences';
import { Filesystem, Directory, Encoding, FilesystemDirectory } from '@capacitor/filesystem';
//import { Storage } from '@ionic/storage';
//import { Storage } from '@ionic/storage-angular';
//import { Storage } from '@capacitor/storage';



@Injectable({
  providedIn: 'root'
})
export class VideoService {
  //public videos = [];
  public videos : string[] = [];
  private VIDEOS_KEY: string = 'videos';

  constructor() { }

  async loadVideos() {
     const videoList:any = await Preferences.get({key: this.VIDEOS_KEY});
     this.videos = JSON.parse(localStorage.getItem(videoList.value) || '{}');
     return this.videos;
  }

  async storeVideo(blob:any) {
    const fileName = new Date().getTime + '.mp4';
    const base64Data = await this.convertBlobToBase64(blob) as string;
    const savedFile = await Filesystem.writeFile({
      path: fileName,
      data:blob,
      directory: FilesystemDirectory.Data,
      encoding: Encoding.UTF8
    });

    this.videos.unshift(savedFile.uri);
    console.log('my array now', this.videos);

    return Preferences.set({
      key: this.VIDEOS_KEY,
      value: JSON.stringify(this.videos)
    });
  }

  private convertBlobToBase64 = (blob: Blob ) => new Promise((resolve, reject) => {
    const reader = new FileReader;
    reader.onerror = reject;
    reader.onload = () => {
      resolve(reader.result);
    };
    reader.readAsDataURL(blob);
  });


  async getVideoUrl(fullPath: any){
    const path = fullPath.substr(fullPath.lastIndexOf('/') + 1);
    const file = await await Filesystem.readFile({
      path: path,
      directory: FilesystemDirectory.Data
    });
    return 'data:video/mp4;base64,${file.data}';

  }



}
